import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class CocosAssignment {

    private static final Map<String, Map<String, Integer>> tweets = new TreeMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        for (int i = 0; i < n; i++) {
            var str = br.readLine();
            insertTweets(str);
        }
        int cant = Integer.parseInt(br.readLine());
        for (int i = 0; i < cant; i++) {
            var str = br.readLine().split(" ");
            var map = solve(Integer.parseInt(str[0]), str[1]);
            Set<String> keys = map.keySet();
            for (String key : keys) {
                System.out.println(key);
            }
        }
    }

    private static void insertTweets(String str) {
        var list = str.split(",");
        var topic = list[0].trim();
        var tweet = list[1].trim();
        var symbol = list[2].trim();
        if(tweets.containsKey(topic)){
            if(tweets.get(topic).containsKey(tweet)){
                if(symbol.equals("+")) {
                    tweets.get(topic).replace(tweet, tweets.get(topic).get(tweet) + 1);
                } else {
                    tweets.get(topic).replace(tweet, tweets.get(topic).get(tweet) - 1);
                }
            } else {
                if(symbol.equals("+")) {
                    tweets.get(topic).put(tweet,1);
                } else {
                    tweets.get(topic).put(tweet,-1);
                }
            }
        } else {
            Map<String, Integer> newMap = new LinkedHashMap<>();
            if(symbol.equals("+")) {
                newMap.put(tweet,1);
            } else {
                newMap.put(tweet,-1);
            }
            tweets.put(topic, newMap);
        }
    }

    private static Map<String, Integer> solve(int number, String topic) {
        var tweet = tweets.get(topic);
        List<Map.Entry<String, Integer>> toSort = new ArrayList<>(tweet.entrySet());
        toSort.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));
        Map<String, Integer> map = new LinkedHashMap<>();
        long limit = number;
        for (Map.Entry<String, Integer> stringIntegerEntry : toSort) {
            if (limit-- == 0) {
                break;
            }
            map.putIfAbsent(stringIntegerEntry.getKey(), stringIntegerEntry.getValue());
        }
        return map;
    }
}
