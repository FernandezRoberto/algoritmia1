import java.util.LinkedList;

public class BreadthFirstSearch {

    private final int vertices;
    private final LinkedList<Integer>[] adjacentList;

    public BreadthFirstSearch(int vertices) {
        this.vertices = vertices;
        adjacentList = new LinkedList[vertices];
        for (int i=0; i<vertices; ++i) {
            adjacentList[i] = new LinkedList();
        }
    }

    public void addEdge(int v,int w) {
        adjacentList[v].add(w);
    }

    public void breadthFirstSearch(int initial) {
        boolean[] visited = new boolean[this.vertices];
        LinkedList<Integer> queue = new LinkedList<>();
        visited[initial] = true;
        queue.add(initial);
        while (queue.size() != 0) {
            initial = queue.poll();
            System.out.print(initial + " ");
            for (int next : adjacentList[initial]) {
                if (!visited[next]) {
                    visited[next] = true;
                    queue.add(next);
                }
            }
        }
    }

    public static void main(String[] args) {
        BreadthFirstSearch breadthFirst = new BreadthFirstSearch(8);
        breadthFirst.addEdge(0, 1);
        breadthFirst.addEdge(0, 2);
        breadthFirst.addEdge(1, 7);
        breadthFirst.addEdge(2, 0);
        breadthFirst.addEdge(2, 3);
        breadthFirst.addEdge(3, 4);
        breadthFirst.addEdge(4, 5);
        breadthFirst.addEdge(4, 6);
        breadthFirst.addEdge(5, 7);
        breadthFirst.addEdge(5, 4);
        breadthFirst.addEdge(6, 7);
        breadthFirst.addEdge(7, 7);

        breadthFirst.breadthFirstSearch(0);
    }
}