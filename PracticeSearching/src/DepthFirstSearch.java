import java.util.*;

public class DepthFirstSearch {

    private final int vertices;
    private final LinkedList<Integer>[] adjacentList;

    public DepthFirstSearch(int vertices) {
        this.vertices = vertices;
        adjacentList = new LinkedList[vertices];
        for (int i = 0; i < vertices; ++i) {
            adjacentList[i] = new LinkedList();
        }
    }

    public void addEdge(int v, int w) {
        adjacentList[v].add(w);
    }

    public void depthFirstSearch(int v, boolean[] visited) {
        visited[v] = true;
        System.out.print(v + " ");
        for (int n : adjacentList[v]) {
            if (!visited[n]) {
                depthFirstSearch(n, visited);
            }
        }
    }

    public int getVertices() {
        return vertices;
    }

    public static void main(String[] args) {
        DepthFirstSearch depthFirst = new DepthFirstSearch(8);
        depthFirst.addEdge(0, 1);
        depthFirst.addEdge(0, 2);
        depthFirst.addEdge(1, 7);
        depthFirst.addEdge(2, 0);
        depthFirst.addEdge(2, 3);
        depthFirst.addEdge(3, 4);
        depthFirst.addEdge(4, 5);
        depthFirst.addEdge(4, 6);
        depthFirst.addEdge(5, 7);
        depthFirst.addEdge(5, 4);
        depthFirst.addEdge(6, 7);
        depthFirst.addEdge(7, 7);

        boolean[] visited = new boolean[depthFirst.getVertices()];
        depthFirst.depthFirstSearch(2, visited);
    }
}
