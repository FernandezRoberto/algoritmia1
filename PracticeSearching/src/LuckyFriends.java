import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class LuckyFriends {
    public static int vertices;
    public static HashMap<String, ArrayList<String>> map = new HashMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        vertices = Integer.parseInt(br.readLine());
        StringBuilder aux = new StringBuilder();
        for (int i = 0; i < vertices; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            String v = st.nextToken();
            String w = st.nextToken();
            aux.append(v).append(w);
            addEdge(v, w);
        }
        String ans = DFS(map,"L");
        StringBuilder resp= new StringBuilder();
        for (int i = 0; i < aux.length(); i++) {
            if(!ans.contains(aux.charAt(i)+"")){
                resp.append(aux.charAt(i)).append(", ");
            }
        }
        System.out.println(resp.substring(0,resp.length()-2));
    }

    public static void addEdge(String v, String w) {
        ArrayList<String> array;
        if (map.containsKey(v)) {
            array = map.get(v);
        } else {
            array = new ArrayList<>();
        }
        array.add(w);
        map.put(v, array);
    }

    static String DFS(HashMap<String, ArrayList<String>> map,String s){
        Stack<String> stack = new Stack<>();
        stack.push(s);
        List<String> seen = new ArrayList<>();
        seen.add(s);
        StringBuilder ans = new StringBuilder();
        while (stack.size() > 0) {
            String vertex = stack.pop();
            ArrayList<String> nodes = map.get(vertex);
            if(nodes != null) {
                for (String w : nodes) {
                    if (!seen.contains(w)) {
                        stack.push(w);
                        seen.add(w);
                    }
                }
            }
            ans.append(vertex);
        }
        return ans.toString();
    }
}
