import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class LuckyNumbersTwo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());
        ArrayList<Integer> numbers = new ArrayList<>();
        int apollo = 0;
        while(st.hasMoreTokens()){
            int aux = Integer.parseInt(st.nextToken());
            if(n > aux){
                numbers.add(aux);
            } else if(n==aux) {
                apollo = apollo + n;
            } else {
                break;
            }
        }
        int lucky = solve(numbers);
        if(apollo>lucky){
            System.out.println("Apollo " + apollo);
        } else if (apollo<lucky){
            System.out.println("Lucky " + lucky);
        } else {
            System.out.println("None");
        }
    }

    private static int solve(ArrayList<Integer> numbers) {
        int max = 0;
        if(numbers.size()>0) {
            for (int i = numbers.size()-1; i >= 0; i--) {
                if(numbers.get(i) == numbers.get(numbers.size()-1)) {
                    max = max + numbers.get(i);
                } else {
                    break;
                }
            }
        } else {
            return 0;
        }
        return  max;
    }
}
