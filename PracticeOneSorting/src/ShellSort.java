public class ShellSort {
    public int[] sort(int[] array) {
        for (int space = array.length/2; space > 0; space /= 2) {
            for (int i = space; i < array.length; i += 1) {
                int aux = array[i];
                int j;
                for (j = i; j >= space && array[j - space] > aux; j -= space) {
                    array[j] = array[j - space];
                }
                array[j] = aux;
            }
        }
        return array;
    }
}
