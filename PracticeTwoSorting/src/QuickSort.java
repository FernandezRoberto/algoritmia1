public class QuickSort {

    public static int[] quicksort(int[] array) {
        quicksort(array, 0, array.length - 1);
        return array;
    }

    private static void quicksort(int[] array, int left, int right) {
        if (left < right) {
            int pivotPosition = partition(array, left, right);
            quicksort(array, left, pivotPosition - 1);
            quicksort(array, pivotPosition + 1, right);
        }
    }

    private static int partition(int[] array, int left, int right) {
        int pivot = array[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (array[j] <= pivot) {
                i++;
                swap(array, i, j);
            }
        }
        swap(array, i + 1, right);
        return i + 1;
    }

    private static void swap(int[] array, int first, int second) {
        int temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }

    static void printArray(int[] array) {
        for (int j : array) {
            System.out.print(j + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array = {200, 157, 56, 9, 356, 25, 135};
        System.out.println("Unsorted array: ");
        printArray(array);
        array = quicksort(array);
        System.out.println("Sorted array: ");
        printArray(array);
    }
}
