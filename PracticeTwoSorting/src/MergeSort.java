public class MergeSort {

    private static int[] mergesort(int[] array) {
        mergesort(array, 0, array.length - 1);
        return array;
    }

    public static void mergesort(int[] array, int left, int right) {
        if (left < right) {
            int middle = left + (right-left)/2;
            mergesort(array, left, middle);
            mergesort(array, middle + 1, right);
            split(array, left, middle, right);
        }
    }

    public static void split(int[] array, int left, int middle, int right) {
        int[] leftArray = new int[middle-left+1];
        int[] rightArray = new int[right-middle];
        for (int i = 0; i < leftArray.length; ++i) {
            leftArray[i] = array[left + i];
        }
        for (int j = 0; j < rightArray.length; ++j) {
            rightArray[j] = array[middle + 1 + j];
        }
        int i = 0, j = 0;
        int k = left;
        while (i < leftArray.length && j < rightArray.length) {
            if (leftArray[i] <= rightArray[j]) {
                array[k] = leftArray[i];
                i++;
            } else {
                array[k] = rightArray[j];
                j++;
            }
            k++;
        }
        while (i < leftArray.length) {
            array[k] = leftArray[i];
            i++;
            k++;
        }
        while (j < rightArray.length) {
            array[k] = rightArray[j];
            j++;
            k++;
        }
    }

    static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array = {200, 157, 56, 9, 356, 25, 135};
        System.out.println("Unsorted array: ");
        printArray(array);
        array = mergesort(array);
        System.out.println("Sorted array: ");
        printArray(array);
    }
}
