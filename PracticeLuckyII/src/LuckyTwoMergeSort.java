import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LuckyTwoMergeSort {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String word = br.readLine();
        for (int pivot = 0; pivot < word.length(); pivot++) {
            String resp = solve(word, pivot);
            System.out.println(resp);
        }
    }

    private static String solve(String word, int i) {
        String answer = "";
        int count = 0;
        String left = word.substring(0,i);
        char letter = word.charAt(i);
        var chars = left.toCharArray();
        chars = mergesort(chars);
        for (int j = 0; j < chars.length; j++) {
            if(chars[j] < letter) {
                answer = answer + " " + chars[j];
                count++;
            } else {
                break;
            }
        }
        return count + answer;
    }

    private static char[] mergesort(char[] array) {
        mergesort(array, 0, array.length - 1);
        return array;
    }

    public static void mergesort(char[] array, int left, int right) {
        if (left < right) {
            int middle = left + (right-left)/2;
            mergesort(array, left, middle);
            mergesort(array, middle + 1, right);
            split(array, left, middle, right);
        }
    }

    public static void split(char[] array, int left, int middle, int right) {
        char[] leftArray = new char[middle-left+1];
        char[] rightArray = new char[right-middle];
        for (int i = 0; i < leftArray.length; ++i) {
            leftArray[i] = array[left + i];
        }
        for (int j = 0; j < rightArray.length; ++j) {
            rightArray[j] = array[middle + 1 + j];
        }
        int i = 0, j = 0;
        int k = left;
        while (i < leftArray.length && j < rightArray.length) {
            if (leftArray[i] <= rightArray[j]) {
                array[k] = leftArray[i];
                i++;
            } else {
                array[k] = rightArray[j];
                j++;
            }
            k++;
        }
        while (i < leftArray.length) {
            array[k] = leftArray[i];
            i++;
            k++;
        }
        while (j < rightArray.length) {
            array[k] = rightArray[j];
            j++;
            k++;
        }
    }
}
