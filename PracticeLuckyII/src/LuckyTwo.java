import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class LuckyTwo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String word = br.readLine();
        for (int pivot = 0; pivot < word.length(); pivot++) {
            String resp = solve(word, pivot);
            System.out.println(resp);
        }
    }

    private static String solve(String word, int i) {
        String answer = "";
        int count = 0;
        String left = word.substring(0,i);
        char letter = word.charAt(i);
        var chars = left.toCharArray();
        //Arrays.sort(chars);
        chars = quicksort(chars);
        for (int j = 0; j < chars.length; j++) {
            if(chars[j] < letter) {
                answer = answer + " " + chars[j];
                count++;
            } else {
                break;
            }
        }
        return count + answer;
    }

    public static char[] quicksort(char[] array) {
        quicksort(array, 0, array.length - 1);
        return array;
    }

    private static void quicksort(char[] array, int left, int right) {
        if (left < right) {
            int pivotPosition = partition(array, left, right);
            quicksort(array, left, pivotPosition - 1);
            quicksort(array, pivotPosition + 1, right);
        }
    }

    private static int partition(char[] array, int left, int right) {
        int pivot = array[right];
        int i = left - 1;
        for (int j = left; j < right; j++) {
            if (array[j] <= pivot) {
                i++;
                swap(array, i, j);
            }
        }
        swap(array, i + 1, right);
        return i + 1;
    }

    private static void swap(char[] array, int first, int second) {
        var temp = array[first];
        array[first] = array[second];
        array[second] = temp;
    }
}
