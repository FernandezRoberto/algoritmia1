import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NavigableSet;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class JulianBirthday { // o(mlog(m))
    public static NavigableSet<Integer> treeSet = new TreeSet<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int max=0;
        for (int i = 0; i < m; i++) {
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            char c = st1.nextToken().charAt(0);
            int num = Integer.parseInt(st1.nextToken());
            if (c == 'A'){
                if(treeSet.size() < n){
                    if (num > max){
                        max = num;
                        treeSet.add(num);
                        System.out.println("YES");
                    } else {
                        System.out.println("NO");
                    }
                } else {
                    System.out.println("NO");
                }
            } else {
                treeSet.remove(num);
                max = treeSet.last();
            }
        }
    }

}
