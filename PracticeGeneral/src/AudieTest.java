import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class AudieTest {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int p = Integer.parseInt(st.nextToken());
        int x = Integer.parseInt(st.nextToken());
        int a = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int y = Integer.parseInt(st.nextToken());
        for (int i = 0; i < p; i++) {
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            var aux = st1.toString().split(" ");
            var positionInSortedArray = getIndex(aux, p, a+1);
            var newSortedList = insertionSort(aux);
            var sum = countSumByLimits(newSortedList, positionInSortedArray);
            if(sum <= m){
                System.out.println("You will be able to return to your home after" + sum +
                        "minutes and you will have to pay 0$ extras");
            } else {
                //do the case for a switching with someone or if its better to go home
            }
        }
    }

    private static int countSumByLimits(String[] newSortedList, int positionInSortedArray) {
        int answer = 0;
        for (int i = 0; i < positionInSortedArray; i++) {
            answer = answer + Integer.parseInt(newSortedList[i]);
        }
        return answer;
    }

    //This is for duplicate values
    public static int getIndex(String[] arr, int n, int idx) {
        int result = 0;
        for (int i = 0; i < n; i++) {
            if (Integer.parseInt(arr[i]) < Integer.parseInt(arr[idx])) {
                result++;
            }
            if (Integer.parseInt(arr[i])  != Integer.parseInt(arr[idx]) || i >= idx) {
                continue;
            }
            result++;
        }
        return result;
    }

    public static String[] insertionSort(String[] array) {
        for (int i = 1; i < array.length; i++) {
            int key = Integer.parseInt(array[i]);
            int j = i - 1;
            while (j >= 0 && Integer.parseInt(array[j]) > key) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key+"";
        }
        return array;
    }
}
