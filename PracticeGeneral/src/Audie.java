import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Audie {
    public static ArrayList<String> myList = new ArrayList<>();
    public static ArrayList<String> listBrands = new ArrayList<>();
    public static LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        for (int i = 0; i < m; i++) {
            listBrands.add(br.readLine());
           var lists = listBrands.get(i).split(",");
            map.put(i, lists.length);
        }
        List<Map.Entry<Integer, Integer> > list = new ArrayList<>(map.entrySet());
        sortLinked(list);
        int days = 0;
        for (Map.Entry<Integer, Integer> l : list) {
            boolean flag = true;
            var brands = listBrands.get(l.getKey()).split(",");
            for (String brand: brands) {
                if(!myList.contains(brand)){
                    myList.add(brand);
                    if(flag){
                        days++;
                        flag = false;
                    }
                }
            }
            if(myList.size()>=n){
                System.out.println(days);
                break;
            }
        }
        if(myList.size()<n){
            System.out.println("you should try another store");
        }
    }

    private static void sortLinked(List<Map.Entry<Integer, Integer>> list) {
        list.sort((entry1, entry2) -> entry2.getValue()
                - entry1.getValue());
    }
}
