import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Timmy {

    public static ArrayList<Integer> list = new ArrayList<>();
    public static ArrayList<Integer> list2 = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int p = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());

        for (int i = 0; i < n; i++) {
            list.add(Integer.parseInt(br.readLine()));
            list2.add(0);
        }
        int resp = solve(n, p, m);
        for (int i : list2) {
            System.out.println(i);
        }
        System.out.println(resp);
    }

    private static int solve(int n, int p, int m) {
        int aux = n/(m+1);
        int indexLast = list.size()-1;
        int indexMove = 0;
        int answer = 0;
        int group = 1;
        for (int i = 0; i < aux; i++) {
            for (int j = 0; j < m+1; j++) {
                if(j!=m){
                    answer = answer + (list.get(indexMove)/p * p);
                    list2.set(indexMove, group);
                } else {
                    if(list.get(indexMove)%p > list.get(indexLast)%p){
                        answer = answer + list.get(indexMove);
                        list2.set(indexMove, group);
                    } else {
                        answer = answer + list.get(indexLast);
                        list2.set(indexLast, group);
                        indexLast--;
                        indexMove--;
                    }
                }
                indexMove++;
            }
            group++;
        }
        while(indexMove <= indexLast){
            answer = answer + (list.get(indexMove)/p * p);
            list2.set(indexMove, group);
            indexMove++;
        }
        return answer;
    }
}
