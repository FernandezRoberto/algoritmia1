import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class AudieTwo {
    public static HashMap<String, HashMap<String, ArrayList<String>>> myCloset = new HashMap<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int s = Integer.parseInt(st.nextToken());
        for (int i = 0; i < n; i++) {
            var str = br.readLine();
            var aux = verifyCloset(str,s);
        }
        for (int i = 0; i < m; i++) {
            var str = br.readLine();
            String answer = verifyCloset(str, s);
            System.out.println(answer);
        }

    }

    private static String verifyCloset(String str, int s) {
        String ans = "";
        var items = str.split(",");
        if(myCloset.containsKey(items[0])){
            if (myCloset.get(items[0]).containsKey(items[1])){
                if(!myCloset.get(items[0]).get(items[1]).contains(items[2])){
                    if (myCloset.get(items[0]).get(items[1]).size() >= s){
                        ans = "You already have: " + myCloset.get(items[0]).get(items[1]).toString();
                    } else {
                        myCloset.get(items[0]).get(items[1]).add(items[2]);
                        ans = "You can keep it";
                    }
                } else {
                    ans = "You should return it";
                }
            } else {
                ArrayList<String> list = new ArrayList<>();
                list.add(items[2]);
                myCloset.get(items[0]).put(items[1],list);
                ans = "You can keep it";
            }
        } else {
            ArrayList<String> list = new ArrayList<>();
            list.add(items[2]);
            HashMap<String, ArrayList<String>> map = new HashMap<>();
            map.put(items[1],list);
            myCloset.put(items[0], map);
            ans = "You can keep it";
        }
        return ans;
    }

}
