import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class bobsAssignment {
    public static Deque<String> deque = new LinkedList<>();
    public static Deque<Integer> values = new LinkedList<>();
    public static int answer;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());

        for (int i = 0; i < n; i++) {
            StringTokenizer st1 = new StringTokenizer(br.readLine());
            var str= st1.nextToken();
            deque.add(str.substring(0,str.length()-1));
            values.add(Integer.parseInt(st1.nextToken()));
        }
        answer = 0;
        boolean resp = solve(m);
        if (resp){
            System.out.println(answer);
        } else {
            System.out.println("No Calculus");
        }
    }

    private static boolean solve(int m) {
        boolean flag = true;
        int noMore = 0;
        boolean bol = false;
        while(answer < m){
           if(flag){
               assert deque.peekFirst() != null;
               if(deque.peekFirst().equals("Calculus")){
                   flag = false;
                   if (noMore == 2){
                       if((answer + values.peekFirst()) < m) {
                           answer = answer + values.removeFirst();
                       } else {
                           break;
                       }
                       deque.removeFirst();
                       noMore = 0;
                       bol = true;
                   } else {
                       noMore++;
                   }
               } else {
                   if((answer + values.peekFirst()) < m) {
                       answer = answer + values.removeFirst();
                   } else {
                       break;
                   }
                   deque.removeFirst();
               }
           } else {
               assert deque.peekLast() != null;
               if(deque.peekLast().equals("Calculus")){
                   flag = true;
                   if (noMore == 2){
                       if((answer + values.peekLast()) < m) {
                           answer = answer + values.removeLast();
                       } else {
                           break;
                       }
                       deque.removeLast();
                       noMore = 0;
                       bol = true;
                   } else {
                       noMore++;
                   }
               } else {
                   if((answer + values.peekLast()) < m) {
                       answer = answer + values.removeLast();
                   } else {
                       break;
                   }
                   deque.removeLast();
               }
           }
        }
        return bol;
    }
}
