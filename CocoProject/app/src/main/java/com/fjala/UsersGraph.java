package com.fjala;

import java.util.ArrayList;
import java.util.HashMap;

public class UsersGraph {

    public HashMap<Integer, ArrayList<Tweets>> tweetsList = new HashMap<>();

    public UsersGraph() {
    }

    public void sortTweetersList(int i) {
        var tweets = tweetsList.get(i);
        String[] cad = new String[tweets.size()];
        for (int j = 0; j < tweets.size(); j++) {
            cad[j] = tweets.get(j).getTweet();
        }
        var x = mergesort(cad);
        ArrayList<Tweets> another = new ArrayList<>();
        for (String str:x) {
            another.add(new Tweets(str));
        }
        tweetsList.replace(i,another);
    }

    private static String[] mergesort(String[] array) {
        mergesort(array, 0, array.length - 1);
        return array;
    }

    public static void mergesort(String[] array, int left, int right) {
        if (left < right) {
            int middle = left + (right-left)/2;
            mergesort(array, left, middle);
            mergesort(array, middle + 1, right);
            split(array, left, middle, right);
        }
    }

    public static void split(String[] array, int left, int middle, int right) {
        String[] leftArray = new String[middle-left+1];
        String[] rightArray = new String[right-middle];
        if (leftArray.length >= 0) {
            System.arraycopy(array, left, leftArray, 0, leftArray.length);
        }
        for (int j = 0; j < rightArray.length; ++j) {
            rightArray[j] = array[middle + 1 + j];
        }
        int i = 0, j = 0;
        int k = left;
        while (i < leftArray.length && j < rightArray.length) {
            if (leftArray[i].compareTo(rightArray[j]) < 0 ) {
                array[k] = leftArray[i];
                i++;
            } else {
                array[k] = rightArray[j];
                j++;
            }
            k++;
        }
        while (i < leftArray.length) {
            array[k] = leftArray[i];
            i++;
            k++;
        }
        while (j < rightArray.length) {
            array[k] = rightArray[j];
            j++;
            k++;
        }
    }

    public void getTweetsList(int i) {
        var tweets = tweetsList.get(i);
        for (Tweets tw: tweets) {
            System.out.print(tw.getTweet()+",");
        }
    }

    public void establishUserLists(int u1, ArrayList<Tweets> list) {
        this.tweetsList.put(u1,list);
    }

    public ArrayList<Integer> updateListIfItHasNSameLists(ArrayList<Integer> list, int n){
        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            var tweets = tweetsList.get(i);
            for (Tweets t: tweets) {
                if(binarySearch(tweetsList.get(0),t) != -1){
                    count++;
                }
            }
            if(count<n){
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public ArrayList<Integer> updateListWithWordsItHas(ArrayList<Integer> list, String word){
        for (int i = 0; i < list.size(); i++) {
            if(countOccurrence(tweetsList.get(list.get(i)),word) == 0){
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public HashMap<Integer, Integer> applyEquation(ArrayList<Integer> list, String word) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (Integer integer : list) {
            var tweets = tweetsList.get(integer);
            int ans = 0;
            int appears = countOccurrence(tweets, word);
            for (int j = 0; j < tweets.size(); j++) {
                if (tweets.get(j).getTweet().contains(word)) {
                    ans = ans + (tweets.size() - j) * appears;
                }
            }
            map.put(integer, ans);
        }
        return map;
    }

    private int countOccurrence(ArrayList<Tweets> tweets, String word) {
        int count = 0;
        for (Tweets t: tweets) {
            count = count + countFreq(word,t.getTweet());
        }
        return count;
    }

    static int countFreq(String word, String str) {
        int M = word.length();
        int N = str.length();
        int res = 0;
        for (int i = 0; i <= N - M; i++) {
            int j;
            for (j = 0; j < M; j++) {
                if (str.charAt(i + j) != word.charAt(j)) {
                    break;
                }
            }
            if (j == M) {
                res++;
            }
        }
        return res;
    }

    public int binarySearch(ArrayList<Tweets> arr, Tweets x) {
        int l = 0, r = arr.size() - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            int res = x.getTweet().compareTo(arr.get(m).getTweet());
            if (res == 0) {
                return m;
            }
            if (res > 0) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return -1;
    }


    public void insertNewNameList(int i, String str) {
        var tweets = tweetsList.get(i);
        boolean flag = false;
        for (int j = 0; j < tweets.size(); j++) {
            if(str.compareTo(tweets.get(j).getTweet())<=0){
                tweets.add(j,new Tweets(str));
                flag = true;
                break;
            }
        }
        if (!flag){
            tweets.add(new Tweets(str));
        }
    }

    public ArrayList<Tweets> findLessNamesInCocosList(String str) {
        var tweets = tweetsList.get(0);
        ArrayList<Tweets> tws = new ArrayList<>();
        for (Tweets tweet : tweets) {
            if (str.compareTo(tweet.getTweet()) > 0) {
                tws.add(tweet);
            }
        }
        return tws;
    }
}
