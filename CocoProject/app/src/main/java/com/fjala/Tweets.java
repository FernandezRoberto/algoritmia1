package com.fjala;

public class Tweets {

    private final String tweet;

    public Tweets(String tweet) {
        this.tweet = tweet;
    }

    public String getTweet() {
        return tweet;
    }
}
