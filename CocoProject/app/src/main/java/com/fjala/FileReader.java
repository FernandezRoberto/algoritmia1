package com.fjala;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class FileReader {

    public void readFile(String path, TweeterGraph tweeterGraph, UsersGraph usersGraph) {
        java.io.FileReader fileReader;
        BufferedReader bufferedReader = null;
        try {
            File file = new File(path);
            fileReader = new java.io.FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            StringTokenizer st = new StringTokenizer(bufferedReader.readLine());
            int n = Integer.parseInt(st.nextToken());
            int cont = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (cont < n) {
                    storeUsers(line, tweeterGraph);
                    cont++;
                } else {
                    storeTweets(line, usersGraph);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != bufferedReader)
                    bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void storeUsers(String line, TweeterGraph tweeterGraph){
        String str = line.substring(0, line.indexOf(" "));
        int u1 = Integer.parseInt(str);
        str = line.substring(line.indexOf(" ") + 1);
        int u2 = Integer.parseInt(str);
        tweeterGraph.addEdge(u1, u2);
    }

    private void  storeTweets(String line, UsersGraph usersGraph){
        String str = line.substring(0, line.indexOf(" "));
        int u1 = Integer.parseInt(str);
        str = line.substring(line.indexOf(" ") + 1);
        var list= str.split(",");
        ArrayList<Tweets> tweetsLists = new ArrayList<>();
        for (String s: list) {
            s = s.strip();
            tweetsLists.add(new Tweets(s));
        }
        usersGraph.establishUserLists(u1, tweetsLists);
    }
}
