package com.fjala;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TweeterGraph {

    public static HashMap<Integer, ArrayList<Integer>> followGraph = new HashMap<>();

    public TweeterGraph() {
    }

    public void addEdge(int u1, int u2) {
        ArrayList<Integer> array;
        if (followGraph.containsKey(u1)) {
            array = followGraph.get(u1);
        } else {
            array = new ArrayList<>();
            if(u1!=0){
                array.add(0);
            }
        }
        array.add(u2);
        followGraph.put(u1, array);
        if (!followGraph.containsKey(u2)) {
            ArrayList<Integer> na = new ArrayList<>();
            na.add(0);
            followGraph.put(u2, na);
        }
    }

    public ArrayList<Integer> usersThatCocoDoesNotFollow(){
        var cocosFriends = followGraph.get(0);
        ArrayList<Integer> noFriends = new ArrayList<>();
        for (Integer key: followGraph.keySet()) {
            if(binarySearch(cocosFriends, key) == -1 && key != 0){
                noFriends.add(key);
            }
        }
        return noFriends;
    }

    public int binarySearch(List<Integer> arr, int x) {
        int l = 0, r = arr.size() - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (arr.get(m) == x) {
                return m;
            }
            if (arr.get(m) < x) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return -1;
    }
}
